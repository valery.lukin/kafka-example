package com.valeryl.kafka.example;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

// May not work properly since example video on handling multithreading was not great.
public class ConsumerWithThread {
    private final Logger LOG = LoggerFactory.getLogger(ConsumerWithThread.class.getName());

    public static void main(String[] args) {
        new ConsumerWithThread().run();
    }

    private void run() {
        String group = "group-2";
        String topic = "first.topic";
        CountDownLatch countDownLatch = new CountDownLatch(1);
        LOG.info("Creating the consumer thread.");
        ConsumerRunnable myConsumerRunnable = new ConsumerRunnable(countDownLatch, topic, group);
        Thread myThread = new Thread(myConsumerRunnable);
        myThread.start();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            LOG.info("Caught shutdown hook");
            myConsumerRunnable.shutdown();
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }));

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            LOG.error("App got interrupted", e);
        } finally {
            LOG.info("App is closing");
        }
    }

    private static class ConsumerRunnable implements Runnable {
        private CountDownLatch latch;
        private final KafkaConsumer<String, String> consumer;
        private static final String BOOTSTRAP_SERVERS = "localhost:9092";
        private final Logger LOG = LoggerFactory.getLogger(ConsumerRunnable.class.getName());

        public ConsumerRunnable(CountDownLatch latch, String topic, String group) {
            this.latch = latch;
            Properties consumerProperties = new Properties();
            consumerProperties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
            consumerProperties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            consumerProperties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            consumerProperties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, group);
            consumerProperties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
            this.consumer = new KafkaConsumer<>(consumerProperties);
            consumer.subscribe(Collections.singleton(topic));
        }

        @Override
        public void run() {
            try {
                while (true) {
                    ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, String> consumerRecord : consumerRecords) {
                        LOG.info("Key: " + consumerRecord.key() + ", Value: " + consumerRecord.value() + "\n");
                        LOG.info("Partition: " + consumerRecord.partition() + "\n" + "Offset: " + consumerRecord.offset() + "\n");
                    }
                }
            } catch (WakeupException ex) {
                LOG.info("Received shutdown signal.");
            } finally {
                consumer.close();
                latch.countDown();
            }
        }

        public void shutdown() {
            consumer.wakeup();
        }
    }
}
