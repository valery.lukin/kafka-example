package com.valeryl.kafka.example;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerWithCallback {
    public static void main(String[] args) {
        Logger LOG = LoggerFactory.getLogger(ProducerWithCallback.class);
        Properties producerProperties = new Properties();
        producerProperties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        producerProperties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producerProperties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<>(producerProperties);

        Callback sendCallback = (RecordMetadata metadata, Exception exception) -> {
            if (exception == null) {
                LOG.info("Received new metadata. \n" +
                        "Topic: " + metadata.topic() + "\n" +
                        "Partition: " + metadata.partition() + "\n" +
                        "Offset: " + metadata.offset() + "\n" +
                        "Timestamp: " + metadata.timestamp() + "\n"
                );
            } else {
                LOG.error("Error producing a message: ", exception);
            }
        };
        for (int i = 0; i < 15; i++) {
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>("first.topic", "Callback message " + i);
            producer.send(producerRecord, sendCallback);
        }
        producer.close();
    }
}
